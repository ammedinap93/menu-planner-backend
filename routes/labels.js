const express = require("express");
const router = express.Router();
const pool = require("../db");

router.get("/", async (req, res) => {
  try {
    const userId = req.user.id;
    const result = await pool.query(
      "SELECT * FROM labels WHERE user_id IS NULL OR user_id = $1",
      [userId]
    );
    res.json(result.rows);
  } catch (error) {
    console.error("Error retrieving labels:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.post("/", async (req, res) => {
  try {
    const { text, color } = req.body;
    const userId = req.user.id;
    const result = await pool.query(
      "INSERT INTO labels (text, user_id, color) VALUES ($1, $2, $3) RETURNING *",
      [text, userId, color]
    );
    res.status(201).json(result.rows[0]);
  } catch (error) {
    console.error("Error creating label:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.put("/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const { text, color } = req.body;
    const userId = req.user.id;
    const result = await pool.query(
      "UPDATE labels SET text = $1, color = $2 WHERE id = $3 AND user_id = $4 RETURNING *",
      [text, color, id, userId]
    );
    if (result.rows.length === 0) {
      return res.status(404).json({ error: "Label not found" });
    }
    res.json(result.rows[0]);
  } catch (error) {
    console.error("Error updating label:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.delete("/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const userId = req.user.id;
    const result = await pool.query(
      "DELETE FROM labels WHERE id = $1 AND user_id = $2 RETURNING *",
      [id, userId]
    );
    if (result.rows.length === 0) {
      return res.status(404).json({ error: "Label not found" });
    }
    res.json({ message: "Label deleted successfully" });
  } catch (error) {
    console.error("Error deleting label:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
